// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA8KF5ZIyiibF4wzifmWngT0fy56SqCv40",
    authDomain: "zweet-556cf.firebaseapp.com",
    databaseURL: "https://zweet-556cf.firebaseio.com",
    projectId: "zweet-556cf",
    storageBucket: "zweet-556cf.appspot.com",
    messagingSenderId: "946829257738"
  }
};
