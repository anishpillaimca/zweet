import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: Observable<firebase.User>;

  constructor(private afAuth: AngularFireAuth, private router: Router) { 
    console.log('LoginComponent Constructor called');
    afAuth.authState.subscribe(auth => {
      if (auth) {
        router.navigate(['']);
      }
    });
    
    // this.user = afAuth.authState;
    // if (this.user) {
    //   router.navigate(['']);
    // }
  }

  ngOnInit() {
  }

  login(form) {
    this.afAuth.auth.signInWithEmailAndPassword(form.value.email, form.value.password);
    form.reset();
  }

  signup(form) {
    this.afAuth.auth.createUserWithEmailAndPassword(form.value.email, form.value.password)  
      .catch(error => {
        alert( error.message);
      });
    form.reset();
  }

}
