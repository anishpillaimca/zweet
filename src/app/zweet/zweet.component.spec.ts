import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZweetComponent } from './zweet.component';

describe('ZweetComponent', () => {
  let component: ZweetComponent;
  let fixture: ComponentFixture<ZweetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZweetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZweetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
