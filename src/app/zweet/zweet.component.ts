import { Component, OnInit } from '@angular/core';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth,FirebaseAuthStateObservable } from 'angularfire2/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-zweet',
  templateUrl: './zweet.component.html',
  styleUrls: ['./zweet.component.scss']
})
export class ZweetComponent implements OnInit {

  items = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  zweets: FirebaseListObservable<any[]>;
  user: any;

  constructor(private afAuth: AngularFireAuth,
    private router: Router,
    private db: AngularFireDatabase) {
    afAuth.authState.subscribe(auth => {
      if (auth) {
        console.log('User:' + auth.email);
        this.user = auth.email;
      } else {
        router.navigate(['login']);
      }
    });
    this.zweets = db.list('/');
   }

  ngOnInit() {
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  add(field) {
    this.zweets.push({
      text: field.value,
      user: this.user,
      createdAt: (new Date()).toString(),
      likes: 0
    });
    field.value = '';
  }

  remove($key: string) {
    this.zweets.remove($key);
  }

  like($key: string,count: number) {
    this.zweets.update($key, {
      likes: count + 1
    });
  }

}
