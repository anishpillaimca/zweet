import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZweetComponent } from './zweet/zweet.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: '',
    component: ZweetComponent
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
